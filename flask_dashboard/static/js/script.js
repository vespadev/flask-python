// Variables
let current_page = window.location.pathname;


// Find current page.
$(".left-navbar").ready(function () {
    let lower;
    $("li").each(function () {
        lower = $(this).find('span').text().toLowerCase();
        if (lower === current_page.substring(1)) {
            $(this).addClass("active");
        }
    });
});