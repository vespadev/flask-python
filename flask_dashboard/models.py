from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_dashboard import db, login_manager, app
from flask_login import UserMixin, login_user, current_user

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique = True)
    email = db.Column(db.String(80), unique = True)
    password = db.Column(db.String(60), nullable=False)
    profile_pic = db.Column(db.String(80), nullable=False, default="default.png")

    def __repr__(self):
        text = self.username + " " + self.email
        return print(text)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(db.Integer, nullable=False, default=1)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    created_by = db.Column(db.Integer, nullable=False)