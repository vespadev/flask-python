from flask import render_template, url_for, flash, redirect
from flask_dashboard import app, db, bcrypt, mail
from flask_dashboard.forms import RegisterForm, LoginForm, ResetPassword, NewPasswordForm, CreateProject
from flask_dashboard.models import User, Projects
from flask_login import login_user, current_user, logout_user, login_required
from flask_mail import Message

@app.route("/", methods=['GET','POST'])
@app.route("/login", methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('home'))
        else:
            flash("Login Unsuccessful.", "danger")
    return render_template("login.html", title=app.name +" | Auth", form=form)


@app.route("/register", methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email = form.email.data, password = hashed_password)
        db.session.add(user)
        db.session.commit()
        text = 'Account created for ' + form.username.data
        flash(text, 'success')
        return redirect(url_for('home'))
    return render_template("register.html", title=app.name +" | Auth", form=form)

@app.route("/create-project", methods=['GET','POST'])
@login_required
def create_project():
    form = CreateProject()
    if form.validate_on_submit():
        project = Projects(title=form.title.data, description=form.description.data, created_by=current_user.id)
        db.session.add(project)
        db.session.commit()
        flash("Project has been created successfully", 'success')
        return redirect(url_for('projects'))
    return render_template("create_project.html", title= app.name + "| Create Project", form=form)

def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                   sender='uhuuu6@vespadev.ro',
                   recipients=[user.email])
    msg.body = "To reset your password, visit:<a href='" + url_for('new_password', token=token, _external=True) +'>Link</a>'
    mail.send(msg)

@app.route("/reset_password", methods=['GET','POST'])
def reset_password():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = ResetPassword()
    if form.validate_on_submit():
        user = User.query.filter_by(email = form.email.data).first()
        send_reset_email(user)
        flash('An email has been send with instructions to reset your password', 'info')
        return redirect(url_for('login'))
    return render_template("reset_password.html", title=app.name +" | Reset Password", form=form)

@app.route("/reset_password/<token>", methods=['GET','POST'])
def new_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash("That is an invalid or expired token", 'warning')
        return redirect(url_for('reset_password'))
    form = NewPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data)
        user.password = hashed_password
        db.session.commit()
        flash("Your password has been updated!", 'success')
        return redirect(url_for('login'))
    return render_template("new_password.html", title=app.name + " | New password", form=form)

@app.route("/profile/<username>", methods=['GET','POST'])
@login_required
def profile(username):
    user_data = User.query.filter_by(username=username).first()
    return render_template("profile.html", title=app.name + " | New password", profile=user_data)

@app.route("/home")
@login_required
def home():
  return render_template("homepage.html", title=app.name +" | Home")

@app.route("/tasks")
@login_required
def tasks():
  return render_template("tasks.html", title=app.name +" | Tasks")

@app.route("/projects")
@login_required
def projects():
    list_of_projects = Projects.query.filter(Projects.created_by==1).all()
    list_of_projects[0].members = [["default.png"],["default.png"]]
    list_of_projects[1].members = [["default.png"],["default.png"]]
    return render_template("projects.html", title=app.name +" | Projects", projects = list_of_projects)

@app.route("/users")
@login_required
def users():
  return render_template("users.html", title=app.name +" | Users")

@app.route("/settings")
@login_required
def settings():
  return render_template("settings.html", title=app.name +" | Settings")

@app.route("/quires")
@login_required
def quires():
  return render_template("quires.html", title=app.name +" | Quires")

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('login'))
